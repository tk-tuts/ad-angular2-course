# TypeScriptAndAngular2Training
Contains projects for the TypeScript and Angular 2 startup training at Trivadis:
http://www.trivadis.com/de/training/einfuehrung-typescript-und-angular-20-ad-angular2


# Prerequisite
1.       Visual Studio Code: https://code.visualstudio.com/Download
2.       Node.js und NPM: https://nodejs.org (LTS version (6.9.1))
3.       Git: https://git-scm.com/downloads 
4.       Clone this repository


```
$ git clone https://github.com/TrivadisCloud/TypeScriptAndAngular2Training.git
```
