// TODO: Create a class that takes
//       firstname, lastName as constructor-parameter
//       and that has a getFullName-method

class Friend {
    firstName: string;
    lastName?: string;

    constructor(firstName: string, lastName?: string) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
}

let friend = new Friend("Keerthikan", "Huber");
console.log(friend.getFullName());