import {Person} from '../app/model/person';

export var PERSONS:Person[]=[
    new Person(1,'Thomas','Huber', 'thomasclaudiushuber'),
    new Person(2,'Julia','Roberts'),
    new Person(3,'Thomas','Bandixen','tbandixen'),
    new Person(4,'Thomas','Gassmann','gassmannT'),
    new Person(5,'Anna'),
    new Person(6,'Trivadis','Cloud','trivadiscloud'),
    new Person(7,'Lara','Croft')
];