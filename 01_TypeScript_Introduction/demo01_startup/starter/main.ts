function sortFriendsByName(friends: IFriend[]) {
    var result = friends.slice(0);

    result.sort((x, y) => {
        return x.firstname.localeCompare(y.firstname);
    });

    return result;
}

interface IFriend {
    firstname: string,
    lastname: string,
}

class Friend implements IFriend {
    firstname: string;
    lastname: string;
    GetFullName(friend: IFriend): string {
        return friend.firstname + ' ' + friend.lastname;
    }
}


let friends: IFriend[] = [
    { firstname: "Z", lastname: "abc" },
    { firstname: "Keerthikan", lastname: "Thurairatnam" },
];
console.log(friends);

let friendsSorted = sortFriendsByName(friends);
console.log(friendsSorted);

